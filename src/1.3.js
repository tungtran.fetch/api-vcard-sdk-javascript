(function ($, CryptoJS) {
    "use strict";
    
    if ('undefined' === (typeof CryptoJS.MD5).toLowerCase()) {
        return (chrome.extension.getBackgroundPage() || window)
            .console.error('This library requires: https://crypto-js.googlecode.com/svn/tags/3.1.2/build/rollups/md5.js');
    }
    
    if ('undefined' === (typeof CryptoJS.AES).toLowerCase()) {
        return (chrome.extension.getBackgroundPage() || window)
            .console.error('This library requires: https://crypto-js.googlecode.com/svn/tags/3.1.2/build/rollups/aes.js');
    }
    
    if ('undefined' === (typeof CryptoJS.HmacSHA1).toLowerCase()) {
        return (chrome.extension.getBackgroundPage() || window)
            .console.error('This library requires: https://crypto-js.googlecode.com/svn/tags/3.1.2/build/rollups/hmac-sha1.js');
    }
    
    if ('undefined' === (typeof CryptoJS.enc.Base64).toLowerCase()) {
        return (chrome.extension.getBackgroundPage() || window)
            .console.error('This library requires: https://crypto-js.googlecode.com/svn/tags/3.1.2/build/components/enc-base64-min.js');
    }
    
    var fn = function (username, password, options) {
        
        this.version = '1.3';
        
        if ('string' !== (typeof username).toLowerCase()) {
            options = username;
            username = null;
            password = null;
            options.public = true;
        } else {
            options.public = false;
        }
        
        var _tokens = {request: {token: null}, access: {token: null}}, _urls = {},
            // If the API consumption is public, query parameters, will placed in this parameter.
            _publicQueryParam = '_d';
        
        _urls.OAuth = {
            request: '/sg/v1/oauth/request/token',
            access: '/sg/v1/oauth/access/token'
        };
        
        options = $.extend({
            verbose: false,
            server: 'https://api.mmvpay.com',
            
            consumer: {
                key: null,
                secret: null
            },

            'method': 'HMAC-SHA1',

            nonce: function () {
                return CryptoJS.MD5((new Date()).toString() + Math.random()).toString();
            },

            timestamp: function () {
                return Math.floor(Date.now() / 1000);
            },
            version: '1.0'
        }, options);

        var _msg = false !== options.verbose ? {
                open: function () {
                    var _obj = options.verbose();
                    return _obj.console.groupCollapsed.apply(_obj.console, arguments);
                },
                
                log: function () {
                    var _obj = options.verbose();
                    return _obj.console.log.apply(_obj.console, arguments);
                },
                
                close: function () {
                    var _obj = options.verbose();
                    return _obj.console.groupEnd.apply(_obj.console, arguments);
                }
            }: {
                open: function () {},
                log: function () {},
                close: function () {}
            };

        _msg.h1 = function () {
            var color = 'color:#060; font-weight:bold; font-size:12pt',
                args = $.map(arguments, function(value, index) {
                    return [value];
                }),
                message = args.shift();
            return _msg.open.apply(this, ["%c " + message, color].concat(args));
        };
        
        _msg.h2 = function () {
            var color = 'color:#36f; font-weight:bold; font-size:10pt',
                args = $.map(arguments, function(value, index) {
                    return [value];
                }),
                message = args.shift();
            return _msg.open.apply(this, ["%c " + message, color].concat(args));
        };
        
        var _util = {
            sort: function (o, fn) {
                
                var sorted = {}, key, a = [];
            
                for (key in o) {
                    if (o.hasOwnProperty(key)) {
                        a.push(key);
                    }
                }
            
                if (fn) {
                    a.sort(fn);
                }
                else {
                    a.sort();
                }
                
            
                for (key = 0; key < a.length; key++) {
                    sorted[a[key]] = o[a[key]];
                }
                
                _msg.log('Sort: %o', sorted);
                return $.extend({}, sorted);
            },
            
            encode: function (str) {
                if (!str) {
                    return '';
                }
                var result = encodeURIComponent(str)
                    .replace(/\*/g, '%2A')
                    .replace(/!/g, '%21')
                    .replace(/'/g, '%27')
                    .replace(/\(/g, '%28')
                    .replace(/\)/g, '%29');
                
                return result;
            },
            
            encrypt: {
                aes: function (secret, signatureBase64, data) {
                    _msg.log('Append the [API Secret] to the URI encoded `signature`');
                    
                    _msg.open('Hash the result using MD5');
                    var key = CryptoJS.MD5(encodeURI(signatureBase64) + secret.join('')).toString();
                    _msg.log('String: %s', key);
                    _msg.close();
                    
                    _msg.open('Encrypt the data using AES 256, mode CBC, Padding PKCS7 with the MD5 as the `key`');
                    var encrypted = CryptoJS.AES.encrypt(data, key, {mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7});
                    _msg.log('Result in Base64: %s', encrypted.toString());
                    _msg.close();
                    
                    return encrypted.toString();
                },
                
                hmac: function (method, url, params, secret, token) {
                    _msg.log('Encode the [method], [url] and the [query] parameters using RFC-3986. ref: www.rfc-base.org/rfc-3986.html');
                    var contents = [
                            _util.encode(method),
                            _util.encode(url),
                            _util.encode($.param(params).replace(/\+/g,'%20'))
                        ];
                    
                    _msg.log('Join the `payload` (method, url, query) with `&`');
                    
                    _msg.log('Encode the [API Secret] using RFC-3986');
                    var key = [_util.encode(secret.join(''))];
                    
                    if (token) {
                        _msg.open('Encode the [OAuth ' + (token == _tokens.request.secret.join('') ? 'Request': 'Access') + ' Token Secret] using RFC-3986');
                        key.push(_util.encode(token));
                        _msg.close();
                        
                        _msg.log('Join the `secret` (API Secret, OAuth ' + (token == _tokens.request.secret.join('') ? 'Request': 'Access') + ' Token Secret) with `&`');
                    }
                    
                    key = key.join('&');
                    
                    _msg.open('Encrypt `payload` with key `secret` using HMAC-SHA1');
                    
                    var signature = CryptoJS.HmacSHA1(contents.join('&'), key).toString(CryptoJS.enc.Base64);
                    _msg.log('signature: %s', signature);
                    _msg.close();
                    
                    return signature;
                }
            }
            
        };
    
        var _connect = function (username, password, options, method) {
            
            method = method || 'POST';
            
            _msg.h1('Process: Requesting for an [OAuth Request Token]');
            
            var url = options.server + _urls.OAuth.request;
            
            _msg.h2('Step 1: Gather [parameters]');
            
            var query = {
                'oauth_consumer_key': options.consumer.key,
                'oauth_nonce': options.nonce(),
                'oauth_signature_method': options.method,
                'oauth_timestamp': options.timestamp(),
                'oauth_version': options.version,
                'oauth_user_name': username,
                'oauth_user_password': password
            };
            _msg.log('Parameters: %o', query);
            _msg.close();
            
            _msg.h2('Step 2: Sort [parameters] ascendingly according to keys');
            query = _util.sort(query);
            _msg.close();
            
            _msg.h2('Step 3: Generate [parameters] payload `signature`');
            var signatureBase64 = _util.encrypt.hmac(method, url, query, options.consumer.secret, '');
            query['oauth_signature'] = encodeURI(signatureBase64);
            _msg.close();
            
            _msg.h2('Step 4: Encrypt the user\'s username');
            query['oauth_user_name'] = _util.encrypt.aes(options.consumer.secret, signatureBase64, username);
            _msg.close();
            
            _msg.h2('Step 5: Encrypt the user\'s password');
            query['oauth_user_password'] = _util.encrypt.aes(options.consumer.secret, signatureBase64, password);
            _msg.close();
            
            _msg.h2('Step 6: Combine the `signature`, `ecrypted username` and the `ecrypted password` to the rest of the [parameters]');
            _msg.log('parameters: %o', query);
            _msg.close();
            
            _msg.h2('Step 7: Send the parameters to %s using the %s method', url, method);
            _msg.log('Sending %s Data: %o to %s', method, query, url);
            _msg.close();
            
            _msg.close(); // H1
            
            return $.ajax({
                url: url,
                crossDomain:true,
                type: method,
                data: query,
                dataType: 'json'
            }).done(function(data) {
                _tokens.request = {
                    token: data.oauth_token,
                    secret: data.oauth_token_secret.split('')
                };
            });
        };
    
        options.consumer.secret = options.consumer.secret.split('');
        
        var _this = this;
        var _connected = false;
        
        this.connect = function () {
            if (_connected) {
                return false;
            }
            
            _connected = true;
            return _connect(username, password, options, 'POST');
        };
        
        this.isLoaded = function () {
            return _tokens.request.token && _tokens.access.token;
        };
        
        this.getTokens = function () {
            return _tokens;
        };
        
        this.setTokens = function (tokens) {
            _tokens = $.extend(_tokens, tokens);
            
            return this;
        };
        
        this.getAccess = function () {
            var method = 'POST', url = options.server + _urls.OAuth.access;
            
            _msg.h1('Process: Requesting for an [OAuth Access Token]');
            _msg.h2('Step 1: Gather [parameters]');
            var query = {
                'oauth_consumer_key': options.consumer.key,
                'oauth_nonce': options.nonce(),
                'oauth_signature_method': options.method,
                'oauth_timestamp': options.timestamp(),
                'oauth_token': _tokens.request.token,
                'oauth_version': options.version
            };
            
            _msg.log('Add the [OAuth Request Token] to the [parameters]');
            _msg.log('Parameters: %o', query);
            _msg.close();
            
            _msg.h2('Step 2: Sort [parameters] ascendingly according to keys');
            _util.sort(query);
            _msg.close();
            
            _msg.h2('Step 3: Generate [parameters] payload `signature`');
            var signatureBase64 = _util.encrypt.hmac(method, url, query, options.consumer.secret, _tokens.request.secret.join(''));
            query['oauth_signature'] = encodeURI(signatureBase64);
            _msg.close();
            
            _msg.h2('Step 4: Combine the `signature` to the rest of the [parameters]');
            _msg.log('parameters: %o', query);
            _msg.close();
            
            _msg.h2('Step 5: Send the parameters to %s using the %s method', url, method);
            _msg.log('Sending %s Data: %o to %s', method, query, url);
            _msg.close();
            
            _msg.close(); // H1
            
            return $.ajax({
                url: url,
                crossDomain:true,
                type: method,
                data: query,
                dataType: 'json'
            }).done(function(data) {
                _tokens.access = {
                    token: data.oauth_token,
                    secret: data.oauth_token_secret.split('')
                };
            });
        };
        
        this.consume = function (url, method, params) {
            
            url = options.server + '/' + url.replace(/^\/*?/, '');
            method = method.toUpperCase();
            
            _msg.h1('Process: Requesting %s Resource [%s]', method, url);
            
            _msg.h2('Step 1: Gather [parameters]');
            var _params = {
                'oauth_consumer_key': options.consumer.key,
                'oauth_nonce': options.nonce(),
                'oauth_signature_method': options.method,
                'oauth_timestamp': options.timestamp(),
                'oauth_token': _tokens.access.token,
                'oauth_version': options.version
            }

            if (options.public) {
                delete _params.oauth_token;
                var tempParams = $.extend({}, params);
                params = {};
                params[_publicQueryParam] = decodeURIComponent($.param(tempParams));
                _msg.log('Assign the parameter in querystring form  to `_d`');
            }
            else
            {
                _msg.log('Add the [OAuth Access Token] to the [parameters]');
            }
            var query = $.extend(_params, params);
            
            
            _msg.log('Parameters: %o', query);
            _msg.close();
            
            _msg.h2('Step 2: Sort [parameters] ascendingly according to keys');
            query = _util.sort(query);
            _msg.close();
            
            _msg.h2('Step 3: Generate [parameters] payload `signature`');
            _params = [method, url, query, options.consumer.secret];
            
            if (!options.public) {
                _params.push(_tokens.access.secret.join(''));
            }
            
            var signatureBase64 = _util.encrypt.hmac.apply(_util.encrypt.hmac, _params);

            query['oauth_signature'] = encodeURI(signatureBase64);
            _msg.close();
            
            var stepCount = 4;
            
            if (options.public) {
                _msg.h2('Step ' + (stepCount++) + ': Encrypt the parameter `_d`');
                query[_publicQueryParam] = _util.encrypt.aes(options.consumer.secret, signatureBase64, query[_publicQueryParam]);
                _msg.close();
            }
            
            _msg.h2('Step ' + (stepCount++) + ': Combine the `signature` to the rest of the [parameters]');
            _msg.log('parameters: %o', query);
            _msg.close();
            
            _msg.h2('Step ' + (stepCount++) + ': Send the parameters to %s using the %s method', url, method);
            _msg.log('Sending %s Data: %o to %s', method, query, url);
            _msg.close();
            
            _msg.close();
            
            return $.ajax({
                url: url,
                crossDomain:true,
                type: method,
                data: query,
                dataType: 'json'
            });
        };
    };
    
    if ('undefined' === (typeof MatchMove).toLowerCase()) {
        window.MatchMove = {};
    }
    
    return MatchMove.WalletRequest = fn;
})(jQuery, CryptoJS);